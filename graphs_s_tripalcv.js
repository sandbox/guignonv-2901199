/**
 * @file
 * Graphs sourcer for Tripal CV Javascript library.
 */

(function ($) {

Drupal.behaviors.graphs_s_tripalcv = {
  attach: function (context, settings) { },

  detach: function (context, settings) { },

  /**
   * Prepare additional parameters for getGraph.
   */
  prepareGetGraphParameters: function (parameters) {
    var sourcer = Drupal.settings.graphs.sourcers[parameters.sourcer_id];
    parameters.graph_type = sourcer.graph_type;
  },

};

}(jQuery));
